from django.shortcuts import render
from slipcrate.packages.models import (
	Package,
	Release,
)



def homepage(request):
	context = {}

	context['featured_releases'] = Release.approved.order_by('-created_at')[:5]

	# the the four newest packages
	context['latest_releases'] = Release.approved.order_by('-created_at')[:5]

	return render(request, 'home.html', context=context)



def help(request):
	context = {}
	return render(request, 'help.html', context=context)
