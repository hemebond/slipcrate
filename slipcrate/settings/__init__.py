"""
Django settings for slipcrate project.
"""

import environ
from pathlib import Path
from django.core.exceptions import ImproperlyConfigured



# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent.parent



env = environ.Env(
	DEBUG=(bool, False)
)
environ.Env.read_env(BASE_DIR / '.env')



SITE_ID = 1
DEBUG = env('DEBUG', False)
SECRET_KEY = env('SECRET_KEY')
# WEBSITE_HOSTNAME is an Azure webapp environment variable
ALLOWED_HOSTS = [
	hostname
	for hostname
	in env.list('ALLOWED_HOSTS', default=['localhost']) + [env('WEBSITE_HOSTNAME', default='')]
	if hostname
]

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'



INSTALLED_APPS = [
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'django.contrib.sites',

	'allauth',
	'allauth.account',
	'django_comments',
	'rest_framework',
	'taggit',
	'widget_tweaks',
	'imagekit',
	'qurl_templatetag',

	'slipcrate.packages',
]

MIDDLEWARE = [
	'django.middleware.security.SecurityMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	'whitenoise.middleware.WhiteNoiseMiddleware',
]

ROOT_URLCONF = 'slipcrate.urls'

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [
			BASE_DIR / 'slipcrate' / 'templates',
		],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
			],
			# Add project-level template tags
			'libraries': {
				'slipcrate': 'slipcrate.templatetags.slipcrate',
			}
		},
	},
]

WSGI_APPLICATION = 'slipcrate.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
DATABASE_URL = env('DATABASE_URL', default=None)

try:
	DATABASES = {
		'default': env.db_url()
	}
except ImproperlyConfigured:
	DATABASES = {
		'default': {
			'ENGINE': env('DATABASE_ENGINE', default='django.db.backends.postgresql'),
			'NAME': env('DATABASE_NAME', default=None),
			'HOST': env('DATABASE_HOST', default=None),
			'USER': env('DATABASE_USER', default=None),
			'PASSWORD': env('DATABASE_PASS', default=None),
			'OPTIONS': {
				'sslmode': 'require'
			}
		}
	}


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
	{'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator', },
	{'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator', },
	{'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator', },
	{'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator', },
]

AUTHENTICATION_BACKENDS = [
	# Needed to login by username in Django admin, regardless of `allauth`
	'django.contrib.auth.backends.ModelBackend',
	# `allauth` specific authentication methods, such as login by e-mail
	'allauth.account.auth_backends.AuthenticationBackend',
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/
LANGUAGE_CODE = 'en'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True



# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATIC_ROOT = env('STATIC_ROOT', default=BASE_DIR / 'static')  # this is where static files will be collected
STATICFILES_DIRS = [
	BASE_DIR / 'slipcrate' / 'static'  # Add the project static files to the search path
]
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'



# Save all uploads to the project directory during development
# DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'  # AWS
# DEFAULT_FILE_STORAGE = 'slipcrate.storages.CustomS3Boto3Storage' # Custom Azure storage handler
DEFAULT_FILE_STORAGE = env('DEFAULT_FILE_STORAGE', default='django.core.files.storage.FileSystemStorage')
MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR / 'media'



# Slipcrate settings
SLIPCRATE = {
	# 'storage': 'storages.backends.s3boto3.S3Boto3Storage'
}



AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID', default=None)
AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY', default=None)
AWS_STORAGE_BUCKET_NAME = env('AWS_STORAGE_BUCKET_NAME', default='slipcrate')
AWS_QUERYSTRING_AUTH = False
AWS_S3_CUSTOM_DOMAIN = env('AWS_S3_CUSTOM_DOMAIN', default=None)

AZURE_ACCOUNT_NAME = env('AZURE_ACCOUNT_NAME', default=None)
AZURE_ACCOUNT_KEY = env('AZURE_ACCOUNT_KEY', default=None)
AZURE_CONTAINER = env('AZURE_CONTAINER', default='slipcrate')
AZURE_CONNECTION_STRING = env('AZURE_CONNECTION_STRING', default=None)



# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_EMAIL_REQUIRED = True
# ACCOUNT_EMAIL_VERIFICATION = "mandatory"
ACCOUNT_EMAIL_VERIFICATION = "none"  # re-enable to mandatory once figured out Azure email
ACCOUNT_PRESERVE_USERNAME_CASING = False



EMAIL_HOST = env('EMAIL_HOST', default='')
EMAIL_HOST_USER = env('EMAIL_HOST_USER', default='')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD', default='')
EMAIL_PORT = env('EMAIL_PORT', default=0)
EMAIL_USE_TLS = env('EMAIL_USE_TLS', default=False)
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL', default='')



IMAGEKIT_CACHE_BACKEND = 'default'
IMAGEKIT_CACHE_TIMEOUT = 7 * 24 * 60 * 60  # one week



LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'formatters': {
		'verbose': {
			'format': (
				'%(asctime)s [%(process)d] [%(levelname)s] ' +
				'pathname=%(pathname)s lineno=%(lineno)s ' +
				'funcname=%(funcName)s %(message)s'
			),
			'datefmt': '%Y-%m-%d %H:%M:%S'
		},
		'simple': {
			'format': '%(levelname)s %(message)s'
		}
	},
	'handlers': {
		'null': {
			'level': 'DEBUG',
			'class': 'logging.NullHandler',
		},
		'console': {
			'level': 'DEBUG',
			'class': 'logging.StreamHandler',
			'formatter': 'verbose'
		}
	},
	'loggers': {
		'django': {
			'handlers': ['console'],
			'level': env('DJANGO_LOG_LEVEL', default='INFO'),
			'propagate': False,
		},
		'testlogger': {
			'handlers': ['console'],
			'level': 'INFO',
		}
	},
	'root': {
		'handlers': ['console'],
		'level': 'WARNING',
	}
}
