//
// Flag Comment
//
document.getElementById('flagCommentModal').addEventListener('show.bs.modal', function(event) {
	const button = event.relatedTarget;
	const commentId = button.dataset.commentId;
	const modal = event.srcElement;

	const comment_author = document.getElementById('c' + commentId).querySelector('.comment-user').textContent;
	const comment_text = document.getElementById('c' + commentId).querySelector('blockquote').innerHTML;

	modal.querySelector('form').setAttribute('action', '/comments/flag/' + commentId + '/');
	modal.querySelector('.modal-title').textContent;
	modal.querySelector('.comment-text').innerHTML = comment_text;
	modal.querySelector('.comment-author').textContent = comment_author;
	modal.querySelector("input[name='comment_id']").value = commentId;
});

document.getElementById('flagCommentForm').addEventListener('submit', function(event) {
	// Stop the regular form submission
	event.preventDefault();

	const form = event.srcElement;

	function success(data) {
		// hide the current flagCommentModal and show the
		// acknowledgement popup modal
		const modalElement = document.getElementById('flagCommentModal');
		const modal = bootstrap.Modal.getOrCreateInstance(modalElement);

		modalElement.addEventListener('hidden.bs.modal', function(event) {
			const ackModalElement = document.getElementById('ackModal');
			const ackModal = bootstrap.Modal.getOrCreateInstance(ackModalElement);

			// use success colour for the modal
			ackModalElement.querySelector('.modal-header').classList.add('bg-success')
			ackModalElement.querySelector('.modal-header').classList.add('text-white');
			ackModalElement.querySelector('.modal-header button').classList.add('text-light');

			// update the title
			ackModalElement.querySelector('.modal-title').textContent  = 'Success';
			// update the body
			ackModalElement.querySelector('.modal-body').innerHTML = '<p>Post reported</p>';
			// show the modal
			ackModal.show();
		});

		modal.hide();
	}

	const httpRequest = new XMLHttpRequest()
	httpRequest.onreadystatechange = function(data) {
		success(data);
	}
	data = new FormData(form);
	httpRequest.open('POST', form.action);
	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	httpRequest.setRequestHeader('X-CSRFToken', data.get('csrfmiddlewaretoken'));
	httpRequest.send(data)
});

//
// Rating Form
//
function submitRatingForm(form) {
	const XHR = new XMLHttpRequest();

	// Bind the FormData object and the form element
	const FD = new FormData(form);

	// Define what happens on successful data submission
	XHR.addEventListener( "load", function(event) {
		if (XHR.status == 200) {
			createAlert('success', 'Your score has been saved');
		}
		else {
			createAlert('danger', XHR.statusText);
		}
	} );

	XHR.addEventListener( "error", function( event ) {
		createAlert('danger', 'Oops! Something went wrong.');
	} );

	XHR.open("POST", form.action);
	XHR.setRequestHeader('X-CSRFToken', FD.get('csrfmiddlewaretoken'));
	XHR.setRequestHeader('X-NoRedirect', true);
	XHR.send(FD);
}

document.getElementsByName('score').forEach(function(input){
	input.addEventListener('change', function(e){
		submitRatingForm(e.target.form);
	});
})

/*
//
// Screenshot Carousel Image Preloader
//
function preloadNextImage(currentImageId) {
	const carouselImages = document.getElementById('screenshot-carousel').getElementsByClassName('carousel-image');
	const nextImageId = currentImageId + 1;
	if (nextImageId < carouselImages.length) {
		const nextImageElement = carouselImages[nextImageId];
		console.log(nextImageElement);

		if (nextImageElement.complete == false) {
			const nextImage = new Image();
			nextImage.src = nextImageElement.src;
		}
	}
}

document.getElementById('screenshot-carousel').addEventListener('slid.bs.carousel', function(e) {
	preloadNextImage(e.to);
});
preloadNextImage(0);
*/
