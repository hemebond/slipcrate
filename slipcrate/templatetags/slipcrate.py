import os
from django.conf import settings
from django.contrib import messages
from django.template import Library, TemplateSyntaxError
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe
from pathlib import Path
from taggit.utils import edit_string_for_tags



register = Library()



@register.inclusion_tag('packages/includes/tag_toggle_button.html')
def tag_toggle(request, parameter_name, tag_label, tag_value):
	"""
	parameter_name:
		name of the query string parameter

	tag_label:
		name or label to show in the button

	tag_value:
		value used when reconstructing the query string

	{% tag_toggle request 'tag' tag.name tag.slug %}
	"""
	querydict = request.query_params.copy()

	# Unwrap list of comma-separated strings
	querystring_tag_values = querydict.getlist(parameter_name, [])

	if tag_value in querystring_tag_values:
		is_active = True
		querystring_tag_values.remove(tag_value)
	else:
		is_active = False
		querystring_tag_values.append(tag_value)

	querydict.setlist(parameter_name, querystring_tag_values)

	return {
		'querystring': '?' + querydict.urlencode(),
		'active': is_active,
		'label': tag_label,
	}



@register.simple_tag
def qs_update(request, **kwargs):
	querydict = request.query_params.copy()

	for k, v in kwargs.items():
		if v is not None:
			querydict[k] = v
		else:
			querydict.pop(k, 0)

	return querydict.urlencode()



@register.simple_tag
def rating_percentage(rating):
	return rating / 5



@register.simple_tag
def rating_for_user(package, user):
	return package.ratings.get(user=user).rating



@register.simple_tag
def icon_rating_for_user(package, user):
	return package.ratings.get(user=user).rating



@register.simple_tag
def rating_stars(score):
	rounded_score = round(score)
	return mark_safe(f'<span class="rating-{rounded_score}" title="{round(score, 1)}">%s</span>' % icon('star') * rounded_score)



@register.simple_tag
def icon(icon):
	icons_file = Path(settings.STATIC_URL) / 'img' / 'icons.svg'
	return mark_safe(f'<svg class="icon" viewBox="0 0 1 1"><use href="{icons_file}#{icon}"></use></svg>')



@register.simple_tag
def rating_badge(score):
	return mark_safe('<span class="badge rating-bg-%s">%s</span>' % (round(score), round(score)))



@register.filter
def strftags(value):
	"""
	Convert the "tags" value of a model
	into a string for use as form field value, e.g.:
	value="{{ f.value|edit_tags|default_if_none:'' }}"
	"""
	if value is not None and not isinstance(value, str):
		return edit_string_for_tags(value)
	return value



@register.simple_tag
def odir(obj):
	"""
	Returns the object properties and methods as a string to help debug
	"""
	return str(dir(obj))



@register.simple_tag
def details(obj):
	out = ' : '.join([getattr(obj, 'name', ''), str(type(obj)), str(obj)])
	for attribute in dir(obj):
		try:
			out += "\n    %s: %s" % (attribute, str(getattr(obj, attribute)))
		except:
			out += "\n    %s: __error__" % attribute

	return str(out)



@register.filter
@stringfilter
def basename(value):
	return os.path.basename(value)



@register.simple_tag
def get(obj, key):
	return obj.get(key, None)



@register.filter
def thumbnail(image):
	path, ext = os.path.splitext(image.url)
	return "%s.thumb%s" % (path, ext)



@register.inclusion_tag('packages/includes/alert_message.html')
def alert_message(message):
	level_class = {
		messages.DEBUG: "secondary",
		messages.INFO: "primary",
		messages.SUCCESS: "success",
		messages.WARNING: "warning",
		messages.ERROR: "danger",
	}.get(message.level, "primary")

	return {
		'level_class': level_class,
		'message': message,
	}



@register.inclusion_tag("packages/includes/sort_th.html")
def sort_th(request, text, sort_by, icon_asc="sort-up", icon_desc="sort-down"):
	"""
	"sort_by" is the field name with "-" prefix for descending sort
	"title" is the text for the TH element

	if this is in the list but not the last, sort by default
	if this is the last-sorted field, then invert
	"""

	def _parse_sort(s):
		"""
		Parses a sort string and returns the name and whether it's asc (True) or desc (False)
		"""
		if s[0] == '-':
			return (s[1:], False)
		return (s, True)

	# what the TH link will sort
	sort_by_name, sort_by_asc = _parse_sort(sort_by)

	# the current list of sorted fields
	current_list = request.GET.getlist('sort', [])

	# get the current field if it's been sorted on
	for field in current_list:
		if _parse_sort(field)[0] == sort_by_name:
			current_field_name, current_field_asc = _parse_sort(field)

	# remove this field form the current sort list
	new_sort_list = [
		field
		for field
		in current_list
		if _parse_sort(field)[0] != sort_by_name
	]

	# the last field to be sorted
	is_last_sort = False
	if current_list:
		last_sort_name, last_sort_asc = _parse_sort(current_list[-1])

		if sort_by_name != last_sort_name:
			# if this field was _not_ the last to be sorted
			# put it onto the end of the list
			new_sort_list.append(sort_by)
		else:
			# this field was the last/most recent to be sorted
			# so add it to the end but invert its direction
			if last_sort_asc:
				new_sort_list.append("-%s" % sort_by_name)
				sort_by_asc = True
			else:
				new_sort_list.append(sort_by_name)
				sort_by_asc = False

			is_last_sort = True
	else:
		new_sort_list = [sort_by, ]

	# make new sort list into querystring
	querydict = request.GET.copy()
	querydict.setlist('sort', new_sort_list)
	querystring = querydict.urlencode()

	ret = {
		"text": text,
		"querystring": querystring,
		"sort_icon": icon_asc if sort_by_asc else icon_desc,
		"sorted": is_last_sort,
	}
	return ret



@register.filter
def markdown(value, arg=''):
	"""
	Runs Markdown over a given value, optionally using various
	extensions python-markdown supports.

	Syntax::

		{{ value|markdown2:"extension1_name,extension2_name..." }}

	To enable safe mode, which strips raw HTML and only returns HTML
	generated by actual Markdown syntax, pass "safe" as the first
	extension in the list.

	If the version of Markdown in use does not support extensions,
	they will be silently ignored.
	"""
	try:
		import markdown2
	except ImportError:
		if settings.DEBUG:
			raise TemplateSyntaxError("Error in {% markdown %} filter: The python-markdown2 library isn't installed.")
		return value
	else:
		def parse_extra(extra):
			if ':' not in extra:
				return (extra, {})
			name, values = extra.split(':', 1)
			values = dict((str(val.strip()), True) for val in values.split('|'))
			return (name.strip(), values)

		extras = (e.strip() for e in arg.split(','))
		extras = dict(parse_extra(e) for e in extras if e)

		if 'safe' in extras:
			del extras['safe']
			safe_mode = True
		else:
			safe_mode = False

		return mark_safe(markdown2.markdown(value, extras=extras, safe_mode=safe_mode))
markdown.is_safe = True
