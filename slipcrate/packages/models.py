import os
import hashlib
from pathlib import Path

from django.conf import settings
from django.core import validators
from django.core.exceptions import ValidationError
from django.core.files.storage import get_storage_class, default_storage
from django.core.validators import FileExtensionValidator
from django.urls import reverse
from django.db import models
from django.db.models import Avg, Sum, Count, Q, Max
from django.db.models.functions import Coalesce
from django.db.models.signals import m2m_changed
from django.utils.html import format_html, mark_safe
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import now
from django.contrib.auth.models import User
from datetime import date

# Receive the pre_delete signal and delete the file associated with the model instance.
from django.db.models.signals import post_delete, post_save
from django.dispatch.dispatcher import receiver


from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase

from imagekit import ImageSpec, register
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill

from .validators import (
	validate_package_slug,
	validate_pakfilename,
)



def details(obj):
	try:
		print(' : '.join([getattr(obj, 'name', ''), str(type(obj)), str(obj)]))
	except:
		pass

	for attribute in dir(obj):
		try:
			print("    %s: %s" % (attribute, str(getattr(obj, attribute))))
		except:
			print("    %s: __error__" % attribute)



def select_storage():
	app_settings = getattr(settings, 'SLIPCRATE', {})
	storage_class = app_settings.get('storage', None)

	if storage_class is not None:
		return get_storage_class(storage_class)()

	return default_storage



def screenshot_upload_to(instance, filename):
	"""
	Place the screenshots in a subdirectory for the package.
	"""
	return os.path.join(instance._meta.app_label, instance.package.slug, filename)



class PackageAuthors(TaggedItemBase):
	content_object = models.ForeignKey('Package', on_delete=models.CASCADE)



class Package(models.Model):
	approved_by = models.ForeignKey(
		User,
		help_text='User account that approved this package',
		related_name='approved_packages',
		on_delete=models.SET_NULL,
		blank=True,
		null=True,
		db_index=True,
	)
	authors = TaggableManager(
		help_text='Comma-separated list of map and mod authors.',
		verbose_name=_('Authors'),
		through=PackageAuthors,
		related_name='packages_authored',
	)
	created_at = models.DateTimeField(
		help_text='When this package listing was created.',
		default=now,
		editable=False,
		db_index=True,
	)
	created_by = models.ForeignKey(
		User,
		help_text='User who created this package listing.',
		related_name='packages_created',
		verbose_name=_('Created by'),
		on_delete=models.SET_NULL,
		null=True,
	)
	dependencies = models.ManyToManyField(
		'self',
		help_text='Which other packages are required to use this package',
		blank=True,
		symmetrical=False
	)
	description = models.TextField(
		help_text='Description of package',
		blank=True,
	)
	name = models.CharField(
		help_text='Full name or title of the package',
		max_length=128,
		unique=True,
	)
	rating = models.FloatField(
		help_text='Bayesian average of all the user ratings, a value from 1.0 to 5.0',
		blank=True,
		null=True,
		editable=False,
		default=0,
	)
	slug = models.SlugField(
		help_text='A globally unique, short, simple name, used for mod directories or PAK filename',
		max_length=16,
		unique=True,
		validators=[
			validate_package_slug,
		]
	)
	tags = TaggableManager(
		help_text='A comma-separated list of tags',
		blank=True
	)

	def __str__(self):
		return self.name

	class Meta:
		ordering = ('slug',)

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.name)
		super().save(*args, **kwargs)

	def get_absolute_url(self):
		return reverse('packages:detail', kwargs={'slug': self.slug})

	def natural_key(self):
		return self.slug

	def cover_image(self):
		try:
			return self.screenshots.first().image
		except self.DoesNotExist:
			return None

	@property
	def has_releases(self):
		return not self.releases.approved.empty()

	@property
	def is_approved(self):
		return self.approved_by is not None



def calculate_file_hash(file_object):
	"""
	Calculate the md5 hash of an uploaded file
	"""
	ctx = hashlib.md5()

	if file_object.multiple_chunks():
		for data in file_object.chunks(ctx.block_size):
			ctx.update(data)
	else:
		ctx.update(file_object.read())

	return ctx.hexdigest()



def get_package_upload_path(instance, filename):
	"""
	Create the upload path to the new file, e.g., `maps/by-sha256/0/001.../filename.ext`
	"""
	return os.path.join('packages', instance.package.slug, filename)



def validate_package_file(value):
	"""
	"""
	try:
		# In-memory files (files being uploaded) have a content_type
		# attribute whereas files already uploaded do not
		if value.file.content_type != 'application/zip':
			raise ValidationError('File must be a zip')
	except AttributeError:
		pass



class ApprovedReleaseManager(models.Manager):
	def get_queryset(self):
		return super().get_queryset().filter(approved_by__isnull=False)



class Release(models.Model):
	approved_by = models.ForeignKey(
		User,
		help_text='User account that approved this release file',
		related_name='approved_releases',
		on_delete=models.SET_NULL,
		blank=True,
		null=True,
		db_index=True,
	)
	created_at = models.DateField(
		help_text='When was this release created',
		default=date.today,
	)
	description = models.TextField(
		help_text='A description of the file',
		blank=True,
	)
	file = models.FileField(
		help_text='A compressed pak, pk3, or zip file. Filename can only contain "a-z0-9_.+"',
		max_length=256,
		upload_to=get_package_upload_path,
		storage=select_storage,
		validators=[
			FileExtensionValidator(
				allowed_extensions=[
					'pak',
					'pk3',
					'zip',
				]
			),
			validate_pakfilename,
		]
	)
	file_name = models.CharField(
		help_text='filename of the file, e.g., something.zip',
		max_length=128,
		blank=True,
		editable=False
	)
	file_hash = models.CharField(
		help_text='MD5 hash of the file',
		max_length=32,
		blank=True,
		editable=False
	)
	file_size = models.BigIntegerField(
		help_text='File size in bytes',
		blank=True,
		null=True,
		editable=False
	)
	name = models.CharField(
		help_text='Friendy name for the release',
		max_length=32,
		blank=True,
	)
	package = models.ForeignKey(
		Package,
		related_name='releases',
		on_delete=models.CASCADE,
	)
	uploaded_at = models.DateTimeField(
		help_text='When this package file was uploaded',
		auto_now_add=True,
		editable=False,
		db_index=True,
	)
	uploaded_by = models.ForeignKey(
		User,
		help_text='The user who uploaded this package',
		related_name='uploaded_releases',
		on_delete=models.SET_NULL,
		blank=True,
		null=True,
	)

	objects = models.Manager()
	approved = ApprovedReleaseManager()

	class Meta:
		permissions = [
			("approve_release", "Can approve a release"),
		]

	def __str__(self):
		return str(os.path.basename(self.file.name))

	def clean(self, *args, **kwargs):
		if self.file and not self.file_hash:
			self._update_file_details(self)
		super().clean(*args, **kwargs)

	def save(self, *args, **kwargs):
		if self.file and not self.file_hash:
			self._update_file_details(self)
		super().save(*args, **kwargs)

	@property
	def is_approved(self):
		return self.approved_by is not None

	def _update_file_details(self, release):
		"""
		Update the package instance with extra detail about the uploaded file
		"""
		release.file_hash = calculate_file_hash(release.file)
		release.file_name = Path(release.file.name).name
		release.file_size = release.file.size



class Rating(models.Model):
	"""
	A package rating. Scale is 1 to 5.
	"""
	user = models.ForeignKey(
		User,
		help_text='User account who created the rating',
		on_delete=models.CASCADE
	)
	created_at = models.DateTimeField(
		auto_now_add=True
	)
	package = models.ForeignKey(
		Package,
		related_name='ratings',
		on_delete=models.CASCADE
	)
	score = models.PositiveSmallIntegerField(
		help_text='a value from 1 to 5',
		validators=[
			validators.MinValueValidator(1),
			validators.MaxValueValidator(5)
		]
	)

	class Meta:
		unique_together = ['user', 'package']



@receiver(post_save, sender=Rating, dispatch_uid="new_rating")
@receiver(post_delete, sender=Rating, dispatch_uid="del_rating")
def update_package_rating(sender, instance, **kwargs):
	"""
	Update the rating for a package
	when it gains or loses a rating
	"""
	bayesian = calculate_bayesian_average(package=instance.package)
	instance.package.rating = bayesian
	instance.package.save(update_fields=['rating'])



def calculate_bayesian_average(package):
	"""
	https://github.com/SpiritQuaddicted/Quaddicted-reviews/blob/master/index.php#L187

	Taken from the original Quaddicted code. Calculates a
	modified version of the bayesian average of its ratings.
	"""
	packages = Package.objects.annotate(sum_ratings=Coalesce(Sum('ratings__score'), 0)).annotate(num_ratings=Count('ratings'))
	package = packages.get(id=package.id)

	sum_ratings = package.ratings.aggregate(sum=Sum('score'))['sum']

	if sum_ratings and sum_ratings > 0:
		avg_num_ratings = packages.filter(~Q(ratings=None)).aggregate(avg=Avg('num_ratings'))['avg']
		sum_sum_ratings = Rating.objects.aggregate(sum=Sum('score'))['sum']
		sum_num_ratings = Rating.objects.count()

		bayesian = (
			avg_num_ratings * (
				sum_sum_ratings / sum_num_ratings
			) + package.num_ratings * (
				package.sum_ratings / package.num_ratings
			)
		) / (avg_num_ratings + package.num_ratings)

		return round(bayesian, 1)
	else:
		return 0



class Url(models.Model):
	"""
	External URLs for a package.
	"""
	label = models.CharField(
		max_length=32
	)
	url = models.URLField()
	package = models.ForeignKey(
		Package,
		related_name='urls',
		on_delete=models.CASCADE
	)

	def __str__(self):
		return self.label



class Screenshot(models.Model):
	"""
	Packages can have zero or more screenshots associated with it.
	Thumbnails are automatically created.
	"""
	image = models.ImageField(
		help_text="A 16x9 resolution image at least 1024x576",
		upload_to=screenshot_upload_to,
		max_length=256,
		storage=select_storage,
	)
	description = models.CharField(
		max_length=32,
		blank=True,
	)
	package = models.ForeignKey(
		Package,
		related_name='screenshots',
		on_delete=models.CASCADE,
	)

	def __str__(self):
		return os.path.basename(self.image.name)

	def get_absolute_url(self):
		return self.image.url




@receiver(post_save, sender=Release, dispatch_uid="release_updated")
@receiver(post_delete, sender=Release, dispatch_uid="release_deleted")
def package_releases_updated(sender, instance, **kwargs):
	newest_release_date = None

	for release in instance.package.releases.filter(approved_by__isnull=False):
		if newest_release_date is None or instance.created_at > newest_release_date:
			newest_release_date = instance.created_at

	if newest_release_date is not None:
		if newest_release_date != instance.package.created_at:
			instance.package.created_at = newest_release_date
			instance.package.save(update_fields=('created_at',))
