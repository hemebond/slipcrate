from django.utils.translation import gettext_lazy as _
from django.contrib import admin
from .models import (
	Package,
	Rating,
	Release,
	Screenshot,
	Url,
)
from django_comments.models import CommentFlag



@admin.register(CommentFlag)
class CommentFlagAdmin(admin.ModelAdmin):
	list_display = ('user', 'flag', 'flag_date')



@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
	list_display = ('user', 'package', 'score', 'created_at')
	search_fields = ['user', 'package__name']
	list_filter = ('score',)



class ApprovedFilter(admin.SimpleListFilter):
	title = _('approval')
	parameter_name = 'approved'

	def lookups(self, request, model_admin):
		return (
			(True, 'Yes'),
			(False, 'No'),
		)

	def queryset(self, request, queryset):
		if self.value() is None:
			return queryset.all()

		if self.value() == "True":
			return queryset.filter(approved_by__isnull=False)

		if self.value() == "False":
			return queryset.filter(approved_by__isnull=True)



@admin.register(Package)
class PackageAdmin(admin.ModelAdmin):
	list_display = ('name', 'slug', 'created_by', 'created_at', 'is_approved')
	list_filter = (ApprovedFilter,)
	actions = ('approve_package',)
	search_fields = ('name',)
	prepopulated_fields = {
		"slug": ('name',),
	}

	def is_approved(self, package):
		"""
		Add an "is approved" column to the list view
		"""
		return package.is_approved
	is_approved.boolean = True

	def approve_package(self, request, queryset):
		"""
		Mark selected packages as approved
		"""
		packages_updated = queryset.update(approved_by=request.user)

		if packages_updated == 1:
			message_bit = "1 package was"
		else:
			message_bit = "%s packages were" % packages_updated

		self.message_user(request, "%s successfully approved." % message_bit)

	approve_package.short_description = "Approve selected packages"



@admin.register(Release)
class ReleaseAdmin(admin.ModelAdmin):
	list_display = ('id', 'package', 'file', 'is_approved')
	list_filter = (ApprovedFilter,)
	actions = ('approve_release',)

	def is_approved(self, release):
		"""
		Add an "is approved" column to the list view
		"""
		return release.is_approved
	is_approved.boolean = True

	def approve_release(self, request, queryset):
		"""
		Mark selected releases as approved
		"""
		releases_updated = queryset.update(approved_by=request.user)

		if releases_updated == 1:
			message_bit = "1 release was"
		else:
			message_bit = "%s releases were" % releases_updated

		self.message_user(request, "%s successfully approved." % message_bit)

	approve_release.short_description = "Approve selected releases"



@admin.register(Screenshot)
class ScreenshotAdmin(admin.ModelAdmin):
	list_display = ('package', 'image',)



@admin.register(Url)
class UrlAdmin(admin.ModelAdmin):
	list_display = (
		'url',
		'label',
		'package',
	)
