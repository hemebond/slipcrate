import os
import logging
from PIL import Image
from io import BytesIO

from django.db.models.signals import (
	pre_delete,
	post_delete,
	pre_save,
	post_save,
)
from django.dispatch.dispatcher import receiver
from django.core.files.base import ContentFile

from slipcrate.packages.models import Screenshot, Package

from imagekit import ImageSpec
from imagekit.processors import ResizeToFill



log = logging.getLogger(__name__)



def details(obj, label=""):
	print(' : '.join([getattr(obj, 'name', ''), str(type(obj)), str(obj)]))
	for attribute in dir(obj):
		try:
			print("    %s: %s" % (attribute, str(getattr(obj, attribute))))
		except:
			print("    %s: __error__" % attribute)



@receiver(post_delete, sender=Screenshot)
def screenshot_delete(sender, instance: Screenshot, **kwargs):
	"""
	Delete a screenshot.
	"""
	instance.image.delete(False)  # Pass false so FileField doesn't save the model.



class Jpeg(ImageSpec):
	format = 'JPEG'
	options = {'quality': 90}



class Thumbnail(ImageSpec):
	processors = [
		ResizeToFill(width=384, height=216, upscale=False),
	]
	format = 'JPEG'
	options = {'quality': 50}



def convert_to_jpeg(image):
	try:
		if image.file.content_type != "image/jpeg":
			new_img_name, new_img_ext = os.path.splitext(image.name)
			jpeg_generator = Jpeg(source=image.file)
			image.file = jpeg_generator.generate()
			image.name = "%s.jpg" % new_img_name
	except:
		details(image)



@receiver(pre_save, sender=Screenshot)
def process_screenshot(sender, instance: Screenshot, **kwargs):
	try:
		old_img = sender.objects.get(pk=instance.pk).image
		if not old_img == instance.image:
			file_path, file_ext = os.path.splitext(old_img.name)

			try:
				old_img.storage.delete("%s.thumb%s" % (file_path, file_ext))
			except Exception as e:
				log.exception(e)

			old_img.delete(False)
	except sender.DoesNotExist:
		old_img = None

	convert_to_jpeg(instance.image)
