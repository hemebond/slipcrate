from django.urls import path

from . import views

app_name = 'packages'
urlpatterns = [
	path('', views.PackageListAPIView.as_view(), name='list'),
	path('new/', views.package_add, name='new'),
	path('<slug:slug>/', views.PackageDetailView.as_view(), name='detail'),
	path('<slug:slug>/edit/', views.package_edit, name='edit'),
	path('<slug:slug>/releases/', views.ReleaseListView.as_view(), name='release-list'),
	path('<slug:slug>/screenshots/', views.ScreenshotList.as_view(), name='screenshots-list'),
]
