"""
A set of Django REST Framework filters used to filter
querysets based on URL query parameters
"""
from rest_framework.filters import (
    BaseFilterBackend,
)



class PackageAuthorFilter(BaseFilterBackend):
    """
    Custom filter for filtering querysets by package author
    """

    def filter_queryset(self, request, queryset, view):
        """
        Author filtering is done with one or more ?author=... query parameters
        """
        authors = request.query_params.getlist('author')
        for author in authors:
            queryset = queryset.filter(authors__slug=author)

        return queryset



class PackageRatingFilter(BaseFilterBackend):
    """
    Custom filter for filtering querysets by package rating
    """

    def filter_queryset(self, request, queryset, view):
        """
        Rating filtering done with ?min_rating= and ?max_rating= query parameters
        """
        min_rating_param = request.query_params.get('min_rating')
        if min_rating_param:
            try:
                min_rating = int(min_rating_param)
                queryset = queryset.filter(rating__gte=min_rating)
            except ValueError:
                # Tried to parse a non-integer
                pass

        max_rating_param = request.query_params.get('max_rating')
        if max_rating_param:
            try:
                max_rating = int(max_rating_param)
                queryset = queryset.filter(rating__lte=max_rating)
            except ValueError:
                # Tried to parse a non-integer
                pass

        return queryset



class PackageTagFilter(BaseFilterBackend):
    """
    Custom filter for filtering querysets by package tags
    """

    def filter_queryset(self, request, queryset, view):
        """
        Tag filtering is done with one or more ?tag=... query parameters
        """
        tags = request.query_params.getlist('tag')
        for tag in tags:
            queryset = queryset.filter(tags__slug=tag)

        return queryset
