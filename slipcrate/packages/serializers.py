from rest_framework import serializers
from taggit.serializers import TagListSerializerField
from slipcrate.packages.models import (
	Package,
	Rating,
	Release,
	Screenshot,
	Url,
)
from django_comments.models import Comment



class URIField(serializers.URLField):
	requires_context = True

	def to_representation(self, value):
		"""
		Create a full URI if the request object is available
		"""
		value = super().to_representation(value)

		request = self.context.get('request', None)
		if request is not None:
			return request.build_absolute_uri(value)

		return value



class CommentSerializer(serializers.ModelSerializer):
	class Meta:
		model = Comment
		fields = [
			'user_name',
			'comment',
			'submit_date',
		]



class ScreenshotSerializer(serializers.HyperlinkedModelSerializer):
	url = URIField(source='get_absolute_url', read_only=True)
	package = serializers.HyperlinkedRelatedField(
		view_name="packages:detail",
		lookup_field='slug',
		queryset=Package.objects.all()
	)

	class Meta:
		model = Screenshot
		fields = [
			'url',
			'package',
		]



class ScreenshotListingField(serializers.RelatedField):
	def to_representation(self, obj):
		"""
		Return a full URL, domain and all, to the image file
		"""
		return self.context['request'].build_absolute_uri(obj.image.url)



class UrlSerializer(serializers.ModelSerializer):
	class Meta:
		model = Url
		fields = [
			'label',
			'url',
		]



class ReleaseSerializer(serializers.ModelSerializer):
	url = URIField(source='file.url', read_only=True)

	class Meta:
		model = Release
		fields = (
			'created_at',
			'url',
			'file_name',
			'file_hash',
			'file_size',
			'uploaded_at',
		)



class PackageSerializer(serializers.ModelSerializer):
	authors = TagListSerializerField()
	dependencies = serializers.HyperlinkedRelatedField(
		many=True,
		view_name="packages:detail",
		lookup_field="slug",
		lookup_url_kwarg="slug",
		read_only=True,
	)
	releases = ReleaseSerializer(
		many=True,
		read_only=True,
	)
	tags = TagListSerializerField()
	url = serializers.HyperlinkedIdentityField(
		view_name="packages:detail",
		lookup_field="slug",
		lookup_url_kwarg="slug",
	)
	rating = serializers.DecimalField(
		max_digits=None,
		decimal_places=1,
	)
	screenshots = ScreenshotListingField(
		many=True,
		read_only=True,
	)
	urls = UrlSerializer(
		many=True,
		read_only=True,
	)

	class Meta:
		model = Package
		depth = 0
		fields = (
			'authors',
			'created_at',
			'dependencies',
			'name',
			'rating',
			'releases',
			'screenshots',
			'tags',
			'url',
			'urls',
			'is_approved',
		)
