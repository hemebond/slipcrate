from django.test import TestCase
from slipcrate.packages.models import Package



class PackageTests(TestCase):
	def test_simple_package(self):
		simple_package = Package.objects.create(name='Simple Package')
		self.assertEqual(simple_package.slug, 'simple-package')
