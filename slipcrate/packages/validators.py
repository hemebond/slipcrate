from django.core.validators import RegexValidator
from django.utils.regex_helper import _lazy_re_compile
from django.utils.translation import gettext_lazy as _



validate_pakfilename = RegexValidator(
	_lazy_re_compile(r'^[a-z\d][a-z\d\._-]+'),
	_('PAK file names must start with a lower-case letter or number, and can contain lower-case letters, numbers, underscores, periods, or hyphens.'),
	'invalid',
)



validate_package_slug = RegexValidator(
	_lazy_re_compile(r'^[a-z0-9]+\Z'),
	_('Package slug can only contain lower-case letters and numbers'),
	'invalid',
)
