import logging
from django.contrib.auth.decorators import login_required
from django.core.exceptions import (
	ObjectDoesNotExist,
	PermissionDenied,
)
from django.db import transaction
from django.db.models import (
	Count,
)
from django.forms import inlineformset_factory
from django.http import (
	FileResponse,
	HttpResponse,
	HttpResponseForbidden,
	HttpResponseRedirect,
)
from django.shortcuts import (
	render,
	get_object_or_404,
)
from django.urls import reverse

from rest_framework import permissions
from rest_framework.filters import (
	SearchFilter,
	OrderingFilter,
)
from rest_framework.renderers import (
	BrowsableAPIRenderer,
	JSONRenderer,
	TemplateHTMLRenderer,
)
from rest_framework.response import Response
from rest_framework.decorators import (
	api_view,
	renderer_classes,
)
from rest_framework.mixins import (
	CreateModelMixin,
	UpdateModelMixin,
	RetrieveModelMixin,
)
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import (
	FormParser,
	JSONParser,
)
from rest_framework.generics import (
	CreateAPIView,
	GenericAPIView,
	ListAPIView,
	ListCreateAPIView,
	RetrieveUpdateAPIView,
)
from .serializers import (
	PackageSerializer,
	ReleaseSerializer,
	ScreenshotSerializer,
)

from .models import (
	Package,
	Rating,
	Release,
	Screenshot,
	Url,
)
from .forms import (
	RatingForm,
	ReleaseForm,
	PackageForm,
)
from .filters import (
	PackageAuthorFilter,
	PackageRatingFilter,
	PackageTagFilter,
)



log = logging.getLogger(__name__)



def details(obj, label=""):
	print(' : '.join([getattr(obj, 'name', ''), str(type(obj)), str(obj)]))
	for attribute in dir(obj):
		try:
			print("    %s: %s" % (attribute, str(getattr(obj, attribute))))
		except Exception:
			print("    %s: __error__" % attribute)



class ScreenshotList(ListCreateAPIView):
	serializer_class = ScreenshotSerializer
	renderer_classes = [
		TemplateHTMLRenderer,
		JSONRenderer,
		BrowsableAPIRenderer
	]

	def get_queryset(self, *args, **kwargs):
		return Screenshot.objects.filter(
			package__slug=self.kwargs['slug']
		)



class PackageTableHTMLRenderer(TemplateHTMLRenderer):
	"""
	A custom renderer just so we can accept the 'card' format querystring parameter
	"""
	format = 'table'
	template_name = 'packages/package_list.html'



class PackageCardHTMLRenderer(TemplateHTMLRenderer):
	"""
	A custom renderer just so we can accept the 'card' format querystring parameter
	"""
	format = 'card'
	template_name = 'packages/package_cards.html'



class PackageListPaginator(PageNumberPagination):
	page_size = 21
	page_size_query_param = 'page_size'
	max_page_size = 99

	def get_paginated_response(self, data):
		return Response({
			'count': self.page.paginator.count,
			'next': self.get_next_link(),
			'previous': self.get_previous_link(),
			'packages': data,
		})



class PackageListAPIView(ListAPIView):
	"""
	Custom list view for packages
	"""
	serializer_class = PackageSerializer
	renderer_classes = [
		PackageTableHTMLRenderer,
		PackageCardHTMLRenderer,
		JSONRenderer,
		BrowsableAPIRenderer
	]
	pagination_class = PackageListPaginator
	filter_backends = [
		SearchFilter,
		OrderingFilter,
		PackageTagFilter,
		PackageAuthorFilter,
		PackageRatingFilter,
	]
	search_fields = [
		'name',
		'tags__name',
		'authors__name'
	]
	ordering_fields = [
		'name',
		'rating',
		'created_at',
	]
	ordering = [
		'name',
		'-rating',
		'-created_at',
	]


	def get_queryset(self):
		return Package.objects\
		              .prefetch_related('authors', 'tags', 'releases')\
		              .filter(approved_by__isnull=False)


	def list(self, request, *args, **kwargs):
		if request.accepted_renderer.format in ['table', 'card']:
			queryset = self.filter_queryset(self.get_queryset())
			queryset = self.paginator.paginate_queryset(queryset, request)

			context = {
				'filtered_authors': self.request.query_params.getlist('author', []),  # list of authors being filtered on
				'filtered_tags': self.request.query_params.getlist('tag', []),  # list of tags filtered for
				'filtered_term': self.request.query_params.get('search', ''),  # search term being searched for
				'page_format': self.request.query_params.get('format', 'list'),
				'packages': queryset,
				'page': self.paginator.page,  # DRF paginator is a wrapper around the Django Paginator
				'next': self.paginator.get_next_link(),
				'previous': self.paginator.get_previous_link(),
			}
			return Response(context)

		return super().list(request, *args, **kwargs)



class IsCreator(permissions.BasePermission):
	"""
	Object-level permission to only allow owners or administratoer to edit an object.
	"""

	def has_object_permission(self, request, view, obj):
		return obj.created_by == request.user



class IsApprovedAndReadOnly(permissions.BasePermission):
	"""
	Object-level permission to only allow owners and administrators to access objects not approved.
	"""

	def has_object_permission(self, request, view, obj):
		# Read permissions are allowed to any request,
		# so always allow GET, HEAD, or OPTIONS requests
		return obj.is_approved and request.method in permissions.SAFE_METHODS



class IsAdminUser(permissions.IsAdminUser):
	"""
	Override default IsAdminUser because if a security bug.
	https://github.com/encode/django-rest-framework/issues/7117
	"""

	def has_object_permission(self, request, view, obj):
		return self.has_permission(request, view)




class PackageDetailView(RetrieveUpdateAPIView):
	"""
	Custom detail view for packages
	"""
	serializer_class = PackageSerializer
	renderer_classes = [
		TemplateHTMLRenderer,
		JSONRenderer,
		BrowsableAPIRenderer,
	]
	permission_classes = [IsCreator | IsApprovedAndReadOnly | IsAdminUser]
	template_name = 'packages/package_detail.html'


	def get_object(self):
		package = get_object_or_404(
			Package.objects.prefetch_related('screenshots')
			               .prefetch_related('authors')
			               .prefetch_related('urls')
			               .prefetch_related('ratings')
			               .prefetch_related('tags'),
			slug=self.kwargs['slug']
		)
		self.check_object_permissions(self.request, package)
		return package


	def retrieve(self, request, *args, **kwargs):
		package = self.get_object()

		if request.accepted_renderer.format in ['html']:
			context = {
				'package': package,
			}
			return Response(context)

		serializer = self.get_serializer(package)
		return Response(serializer.data)




def package_detail_html(request, slug):
	package = get_object_or_404(
		Package.objects.prefetch_related('screenshots')
		               .prefetch_related('authors')
		               .prefetch_related('urls')
		               .prefetch_related('ratings')
		               .prefetch_related('tags'),
		slug=slug
	)

	if not can_view_package(package, request.user):
		return HttpResponseForbidden()

	#
	# Rating Submission
	#
	# TODO: make this less ugly; maybe ModelForm?
	if request.method == 'POST' and request.user.is_authenticated:
		rating_form = RatingForm(request.POST)

		if rating_form.is_valid():
			score = rating_form.cleaned_data['score']

			rating_obj, created = Rating.objects.update_or_create(
				user=request.user,
				package=package,
				defaults={
					'score': score,
				}
			)

			rating_obj.save()

			if request.headers.get('X-NoRedirect', False):
				# Normally a POST would result in a redirect and GET request
				# so we use a custom header to prevent that when POSTing
				# a form via Javascript
				return HttpResponse()

			return HttpResponseRedirect(package.get_absolute_url())
	else:
		rating_form = RatingForm()

	#
	# Ratings
	#
	# Get the tallies of each rating value (1 to 5)
	rating_dict = {
		r['score']: r['score__count']
		for r
		in package.ratings.all().values('score').annotate(Count('score'))
	}
	rating_list = [
		rating_dict.get(i, 0)
		for i
		in range(1, 6)
	]

	user_rating = None
	if request.user.id:
		try:
			user_rating = package.ratings.get(user=request.user.id).score
		except ObjectDoesNotExist:
			pass

	return render(request, 'packages/package_detail.html', {
		'package': package,
		'active_section': 'packages',
		'num_ratings': package.ratings.count(),
		'rating_list': rating_list,
		'user_rating': user_rating,
		'can_edit': can_edit_package(package, request.user),
	})



@api_view(['GET', 'POST'])
@renderer_classes([TemplateHTMLRenderer, JSONRenderer, BrowsableAPIRenderer])
def package_detail(request, slug):
	requested_format = request.accepted_renderer.format

	if requested_format == 'html':
		return package_detail_html(request, slug=slug)

	package = Package.objects.get(slug=slug)
	serializer = PackageSerializer(package, context={"request": request})

	return Response(serializer.data)



@login_required
def package_add(request):
	ReleaseFormSet = inlineformset_factory(
		Package,
		Release,
		form=ReleaseForm,
		extra=1,
		min_num=0,
		validate_min=True,
	)
	ScreenshotFormSet = inlineformset_factory(
		Package,
		Screenshot,
		exclude=(),
	)
	UrlFormSet = inlineformset_factory(
		Package,
		Url,
		exclude=(),
	)

	if request.method == 'POST':
		package_form = PackageForm(request.POST, request.FILES)
		release_formset = ReleaseFormSet(request.POST, request.FILES)
		screenshot_formset = ScreenshotFormSet(request.POST, request.FILES)
		url_formset = UrlFormSet(request.POST, request.FILES)

		if all([
			package_form.is_valid(),
			release_formset.is_valid(),
			screenshot_formset.is_valid(),
			url_formset.is_valid(),
		]):
			with transaction.atomic():
				for form in release_formset:
					if form.has_changed():
						if form.instance.uploaded_by is None:
							form.instance.uploaded_by = request.user

				package_form.instance.created_by = request.user
				new_package = package_form.save()

				release_formset.instance = new_package
				release_formset.save()

				screenshot_formset.instance = new_package
				screenshot_formset.save()

				url_formset.instance = new_package
				url_formset.save()

			if "_continue" in request.POST:
				return HttpResponseRedirect(
					reverse('packages:edit', kwargs={'slug': new_package.slug})
				)

			return HttpResponseRedirect(new_package.get_absolute_url())
	else:
		package_form = PackageForm()
		release_formset = ReleaseFormSet()
		screenshot_formset = ScreenshotFormSet()
		url_formset = UrlFormSet()

	return render(request, 'packages/package_form.html', {
		'package_form': package_form,
		'release_formset': release_formset,
		'screenshot_formset': screenshot_formset,
		'url_formset': url_formset,
	})




def can_edit_package(package, user):
	if user.has_perm('packages.change_package'):
		return True

	if package.created_by == user:
		return True

	return False



def can_view_package(package, user):
	if package.approved_by != None:
		return True

	if user.has_perm('packages.approve_release'):
		return True

	if package.created_by == user:
		return True

	return False



@login_required
def package_edit(request, slug):
	package = get_object_or_404(
		Package.objects.prefetch_related('screenshots'),
		slug=slug
	)

	if not can_edit_package(package, request.user):
		raise PermissionDenied()

	ReleaseFormSet = inlineformset_factory(
		Package,
		Release,
		form=ReleaseForm,
		extra=1,
		validate_min=True,
	)
	ScreenshotFormSet = inlineformset_factory(
		Package,
		Screenshot,
		exclude=()
	)
	UrlFormSet = inlineformset_factory(
		Package,
		Url,
		exclude=(),
		extra=2
	)

	if request.method == 'POST':
		package_form = PackageForm(request.POST, request.FILES, instance=package)
		release_formset = ReleaseFormSet(
			request.POST,
			request.FILES,
			instance=package,
		)
		screenshot_formset = ScreenshotFormSet(request.POST, request.FILES, instance=package)
		url_formset = UrlFormSet(request.POST, request.FILES, instance=package)

		if all([
			package_form.is_valid(),
			release_formset.is_valid(),
			screenshot_formset.is_valid(),
			url_formset.is_valid(),
		]):
			with transaction.atomic():
				for form in release_formset:
					if form.has_changed():
						if form.instance.uploaded_by is None:
							form.instance.uploaded_by = request.user
				release_formset.save()

				screenshot_formset.save()
				url_formset.save()

				package = package_form.save()

			if "_continue" in request.POST:
				return HttpResponseRedirect(
					reverse('packages:edit', kwargs={'slug': package.slug})
				)

			return HttpResponseRedirect(package.get_absolute_url())

		details(release_formset)
	else:
		package_form = PackageForm(instance=package)
		release_formset = ReleaseFormSet(instance=package)
		screenshot_formset = ScreenshotFormSet(instance=package)
		url_formset = UrlFormSet(instance=package)

	return render(request, 'packages/package_form.html', {
		'package': package,
		'package_form': package_form,
		'release_formset': release_formset,
		'screenshot_formset': screenshot_formset,
		'url_formset': url_formset,
	})



@login_required
def package_screenshot(request, slug, id):
	screenshot = get_object_or_404(Screenshot, id=id)
	return FileResponse(screenshot.image)



class ReleaseListView(ListCreateAPIView):
	model = Release
	serializer_class = ReleaseSerializer

	def get_queryset(self):
		return Release.objects.filter(package__slug=self.kwargs['slug'])
