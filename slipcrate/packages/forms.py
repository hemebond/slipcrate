from django import forms
from taggit.utils import parse_tags
from taggit.forms import TagWidget
from .models import (
	Package,
	Release,
	Url,
)



class TagField(forms.CharField):
	"""
	A custom TagField because of https://github.com/jazzband/django-taggit/issues/749
	"""
	widget = TagWidget

	def to_python(self, value):
		return parse_tags(value)

	def has_changed(self, initial, data):
		print('has_changed(%s, %s)' % (initial, data))
		if not initial:
			initial = []

		initial = [tag.name for tag in initial]
		initial.sort()

		return super().has_changed(initial, data)



class RatingForm(forms.Form):
	score = forms.IntegerField(
		max_value=5,
		min_value=1,
	)



class PackageForm(forms.ModelForm):
	class Meta:
		model = Package
		fields = (
			'authors',
			'description',
			'name',
			'slug',
			'tags',
		)



class ReleaseForm(forms.ModelForm):
	class Meta:
		model = Release
		fields = (
			'created_at',
			'file',
		)

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

		# When showing release inline forms we do not want the user
		# to change the file but do allow deleting or changing the created_at value
		instance = getattr(self, 'instance', None)
		if instance and instance.pk:
			self.fields['file'].disabled = True
